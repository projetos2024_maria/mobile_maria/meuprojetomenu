
import { StyleSheet, Text, View, ScrollView, TouchableOpacity, Button } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Menu from './src/components/menu';
import Components from './src/components/components'
import Layout from './src/layout/layoutTela';
import Saudacoes from './src/exercicios/saudacoes'
import Opacity from './src/exercicios/opacity'
import Contagem from './src/exercicios/contagem'

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
      <Stack.Screen name="Menu" component={Menu}/>
        <Stack.Screen name="Componentes" component={Components}/>
        <Stack.Screen name="LayoutTela" component={Layout}/>
        <Stack.Screen name="Saudacoes" component={Saudacoes}/>
        <Stack.Screen name="Opacidade" component={Opacity}/>
        <Stack.Screen name="Contador" component={Contagem}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}


  

