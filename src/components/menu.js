import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, ScrollView, TouchableOpacity } from 'react-native';



export default function Menu({navigation}) {
  return (
    <View style={styles.container}>
     <TouchableOpacity style={styles.menu} onPress={()=> navigation.navigate('Componentes')}>
    <Text>Opção 1</Text>
     </TouchableOpacity>
     <TouchableOpacity style={styles.menu} onPress={()=> navigation.navigate('Contador')}>  
     <Text>Opção 2</Text>
     </TouchableOpacity>
     <TouchableOpacity style={styles.menu} onPress={()=> navigation.navigate('LayoutTela')} >
     <Text>Opção 3</Text>
     </TouchableOpacity>
     <TouchableOpacity style={styles.menu}  onPress={()=> navigation.navigate('Saudacoes')}>
     <Text>Opção 4</Text>
     </TouchableOpacity>
     <TouchableOpacity style={styles.menu}  onPress={()=> navigation.navigate('Opacidade')}>
     <Text>Opção 5</Text>
     </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: 'center'
  },
  menu:{
    padding: 10,
    margin: 5,
    backgroundColor: 'pink',
    borderRadius: 5,
  }

});
