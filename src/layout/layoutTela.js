import { StatusBar } from "expo-status-bar";
import { StyleSheet, Text, View, ScrollView } from "react-native";

export default function Layout() {
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Text> MENU </Text>
      </View>
      <View style={styles.content}>
        <ScrollView>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text>Maria</Text>
          <Text>Maria</Text>
          <Text>Maria</Text>
          <Text>Maria</Text>
          <Text>Maria</Text>
          <Text>Maria</Text>
          <Text>Maria</Text>
          <Text>Maria</Text>
          <Text>Maria</Text>
          <Text>Maria</Text>
          <Text>Maria</Text>
          <Text>Maria</Text>
          <Text>Maria</Text>
          <Text>Maria</Text>
          <Text>Maria</Text>
          <Text>Maria</Text>
          <Text>Maria</Text>
          <Text>Maria</Text>
          <Text>Maria</Text>
          <Text>Maria</Text>
          <Text>Maria</Text>
          <Text>Maria</Text>
          <Text>Maria</Text>
          <Text>Maria</Text>
          <Text>Maria</Text>
          <Text>Maria</Text>
          <Text>Maria</Text>
          <Text>Maria</Text>
          <Text>Maria</Text>
          <Text>Maria</Text>
          <Text>Maria</Text>
          <Text>Maria</Text>
          <Text>Maria</Text>
          <Text>Maria</Text>
          <Text>Maria</Text>
          <Text>Maria</Text>
          <Text>Maria</Text>
          <Text>Maria</Text>
          <Text>Maria</Text>
          <Text>Maria</Text>
          <Text>Maria</Text>
          <Text>Maria</Text>
          <Text>Maria</Text>
          <Text>Maria</Text>
          <Text>Maria</Text>
          <Text>Maria</Text>
          <Text>Maria</Text>
          <Text>Maria</Text>
          <Text>Maria</Text>
   </ScrollView>
      </View>
      <View style={styles.footer}>
        <Text>RODAPÉ</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "space-between",
  },

  header: {
    height: 50,
    backgroundColor: "pink",
  },

  content: {
    flex: 1,
    backgroundColor: "white",
  },

  footer: {
    height: 50,
    backgroundColor: "pink",
  },

  text: {
    fontSize: 10,
    fontWeight: "bold",
    textAlign: "center",
  },
});
